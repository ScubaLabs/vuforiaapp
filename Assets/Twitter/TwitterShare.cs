using UnityEngine;
using System.Collections;
using TwitterKit.Unity;

public class TwitterShare : MonoBehaviour
{
	void Start ()
	{
	}

	public void startLogin() {
		UnityEngine.Debug.Log ("startLogin()");
		Application.CaptureScreenshot("Screenshot.png");
		// To set API key navigate to tools->Twitter Kit
		Twitter.Init ();

		Twitter.LogIn (LoginCompleteWithEmail, (ApiError error) => {
			UnityEngine.Debug.Log (error.message);
		});
	}
	
	public void LoginCompleteWithEmail (TwitterSession session) {
		UnityEngine.Debug.Log ("LoginCompleteWithEmail()");
		Twitter.RequestEmail (session, RequestEmailComplete, (ApiError error) => { UnityEngine.Debug.Log (error.message); });
	}
	
	public void RequestEmailComplete (string email) {
		UnityEngine.Debug.Log ("email=" + email);
		LoginCompleteWithCompose ( Twitter.Session );
	}
	
	public void LoginCompleteWithCompose(TwitterSession session) {
		
		UnityEngine.Debug.Log ("Screenshot location=" + Application.persistentDataPath + "/Screenshot.png");
        string imageUri = "file://" + Application.persistentDataPath + "/Screenshot.png";
		Twitter.Compose (session, imageUri, "http://i.imgur.com/j4M7vCO.jpg", new string[]{"#TwitterKitUnity"},
			(string tweetId) => { UnityEngine.Debug.Log ("Tweet Success, tweetId=" + tweetId); },
			(ApiError error) => { UnityEngine.Debug.Log ("Tweet Failed " + error.message); },
			() => { Debug.Log ("Compose cancelled"); }
		 );
	}
}