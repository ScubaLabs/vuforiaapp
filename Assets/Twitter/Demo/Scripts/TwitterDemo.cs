﻿using UnityEngine;
using System.Collections;
using TwitterKit.Unity;
using UnityEngine.UI;

public class TwitterDemo : MonoBehaviour
{

    string[] str = new string[3];
    public Text[] t;


    public void startLogin()
    {
        UnityEngine.Debug.Log("startLogin()");
        // To set API key navigate to tools->Twitter Kit
        Twitter.Init();

        Twitter.LogIn(LoginCompleteWithEmail, (ApiError error) => {
            UnityEngine.Debug.Log(error.message);
        });
    }

    public void LoginCompleteWithEmail(TwitterSession session)
    {
        UnityEngine.Debug.Log("LoginCompleteWithEmail()");
        Twitter.RequestEmail(session, RequestEmailComplete, (ApiError error) => { UnityEngine.Debug.Log(error.message); });
    }

    public void RequestEmailComplete(string email)
    {
        UnityEngine.Debug.Log("email=" + email);
        LoginCompleteWithCompose(Twitter.Session);
    }

    public void LoginCompleteWithCompose(TwitterSession session)
    {
        Application.CaptureScreenshot("Screenshot.png");
        UnityEngine.Debug.Log("Screenshot location=" + Application.persistentDataPath + "/Screenshot.png");
        string imageUri = "file://" + Application.persistentDataPath + "/Screenshot.png";
        Twitter.Compose(session, imageUri, "Welcome to", new string[] { "#TwitterKitUnity" },
            (string tweetId) => { UnityEngine.Debug.Log("Tweet Success, tweetId=" + tweetId);
                str[0] += "success";
                t[0].text = str[0];
                t[0].gameObject.SetActive(true);
            },
            (ApiError error) => { UnityEngine.Debug.Log("Tweet Failed " + error.message);
                str[1] += "failed";
                t[1].text = str[1];
                t[1].gameObject.SetActive(true);
            },
            () => { Debug.Log("Compose cancelled");
                str[2] += "cancel";
                t[2].text = str[2];
                t[2].gameObject.SetActive(true);
            }
         );

    }
}
