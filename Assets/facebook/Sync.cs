﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// Include Facebook namespace
using Facebook.Unity;
using System;
using UnityEngine.UI;

public class Sync : MonoBehaviour {


    public Text t; 

    // Awake function from Unity's MonoBehavior
    void Awake()
    {
     //   StartSharing();
    }

    public void StartSharing()
    {
        if (!FB.IsInitialized)
        {
            // Initialize the Facebook SDK
            FB.Init(InitCallback, OnHideUnity);
        }
        else
        {
            // Already initialized, signal an app activation App Event
            InitCallback();
        }

    }

    private void OnHideUnity(bool isGameShown)
    {
        if (!isGameShown)
            Time.timeScale = 0;
        else
            Time.timeScale = 1;
    }

    private void InitCallback()
    {
        if (FB.IsInitialized)
        {
            // Signal an app activation App Event
            FB.ActivateApp();
            // Continue with Facebook SDK
            var permissions = new List<string>();
            permissions.Add("public_profile");
            permissions.Add("email");
            permissions.Add("user_friends");

            FB.LogInWithReadPermissions(permissions, AuthCallback);
            
        }
        else
        {
            Debug.Log("Failed to Initialize the Facebook SDK");
        }
    }
    
    private void AuthCallback(ILoginResult result)
    {
        if (FB.IsLoggedIn)
        {
            // AccessToken class will have session details
            var aToken = Facebook.Unity.AccessToken.CurrentAccessToken;
            // Print current access token's User ID
            Debug.Log(aToken.UserId);
            // Print current access token's granted permissions
            foreach (string perm in aToken.Permissions)
            {
                Debug.Log(perm);
            }
            FB.ShareLink(new Uri("http://i.imgur.com/j4M7vCO.jpg"), "test photo", "no description", callback:ShareCallback);
            //FB.ShareLink(new Uri("https://developers.facebook.com/"),callback: ShareCallback);
        }
        else
        {
            Debug.Log("User cancelled login");
        }
    }

    

private void ShareCallback(IShareResult result)
    {
        if (result.Cancelled || !String.IsNullOrEmpty(result.Error))
        {
            Debug.Log("ShareLink Error: " + result.Error);
            t.text = "error" + result.Error;
        }
        else if (!String.IsNullOrEmpty(result.PostId))
        {
            // Print post identifier of the shared content
            Debug.Log(result.PostId);
            t.text = result.PostId;
            
        }
        else
        {
            // Share succeeded without postID
            Debug.Log("ShareLink success!");
            t.text = "success";
            
        }
        t.gameObject.SetActive(true);
        //FB.LogOut();
    }
}
