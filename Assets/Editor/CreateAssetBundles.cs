﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEditor;

public class CreateAssetBundles : EditorWindow
{
	
	[MenuItem ("Assets/Build AssetBundles")]
	static void BuildAllAssetBundles ()
	{
		
		BuildPipeline.BuildAssetBundles("Assets/AssetsBundle/", BuildAssetBundleOptions.UncompressedAssetBundle, BuildTarget.Android);
		
		
	}
}