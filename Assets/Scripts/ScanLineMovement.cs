﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScanLineMovement : MonoBehaviour
{
    public float downLimit = -0.75f;

    public float upLimit = 0.75f;

    private Boolean isMovingUp;

    public float scanningSpeed = 0.01f;

    // Use this for initialization
    void Start()
    {
        StartCoroutine(MoveScanLine());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator MoveScanLine()
    {
        Debug.Log("y position"+transform.position.y);

        //if (isMovingUp )
        //{
            while (transform.position.y < upLimit)
            {
                transform.position = new Vector3(transform.position.x,
              
                    transform.position.y * scanningSpeed * Time.deltaTime, transform.position.z);
                Debug.Log("New Position"+transform.position);
                yield return null;
            }
            
            isMovingUp = false;/*
        }
        else
        {
            while (transform.position.y>downLimit)
            {
                transform.position = new Vector3(transform.position.x,
                    -transform.position.y * scanningSpeed * Time.deltaTime, transform.position.z);
            }
            isMovingUp = true;

        }*/
    }
}