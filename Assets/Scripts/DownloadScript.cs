﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class DownloadScript : MonoBehaviour
{

	

	private WWW Url;

	private VideoPlayer Player;
	private AudioSource audioSource;
	public RawImage image;
	private VideoClip videoToPlay;
	
	// Use this for initialization
	void Start ()
	{
		Application.runInBackground = true;
		



	}
public IEnumerator playVideo()
    {
      
        //Add VideoPlayer to the GameObject
        Player = gameObject.AddComponent<VideoPlayer>();
 
        //Add AudioSource
        audioSource = gameObject.AddComponent<AudioSource>();
 
        //Disable Play on Awake for both Video and Audio
        Player.playOnAwake = false;
        audioSource.playOnAwake = false;
        audioSource.Pause();
 
        //We want to play from video clip not from url
        
      //  Player.source = VideoSource.Url;
 
        // Vide clip from Url
	    Player.source = VideoSource.Url;
	    Player.url = "http://www.quirksmode.org/html5/videos/big_buck_bunny.mp4";
 
 
        //Set Audio Output to AudioSource
        Player.audioOutputMode = VideoAudioOutputMode.AudioSource;
 
        //Assign the Audio from Video to AudioSource to be played
        Player.EnableAudioTrack(0, true);
        Player.SetTargetAudioSource(0, audioSource);
 
        //Set video To Play then prepare Audio to prevent Buffering
        //Player.clip = videoToPlay;
        Player.Prepare();
 
        //Wait until video is prepared
        WaitForSeconds waitTime = new WaitForSeconds(1);
        while (!Player.isPrepared)
        {
            Debug.Log("Preparing Video");
            //Prepare/Wait for 5 sceonds only
            yield return waitTime;
            //Break out of the while loop after 5 seconds wait
            break;
        }
 
        Debug.Log("Done Preparing Video");
 
        //Assign the Texture from Video to RawImage to be displayed
        image.texture = Player.texture;
 
        //Play Video
       Player.Play();
	  
 		
        Debug.Log("Playing Video");
	    Player.isLooping = true;
	    
        Debug.Log("Done Playing Video");
    }
}

