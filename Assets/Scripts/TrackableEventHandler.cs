﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using Vuforia;

public class TrackableEventHandler : MonoBehaviour, ITrackableEventHandler
{
    #region PRIVATE_MEMBER_VARIABLES
 
    private TrackableBehaviour mTrackableBehaviour;
    
    #endregion // PRIVATE_MEMBER_VARIABLES
    
    public GameObject ObjectToDetect;
    private Transform _originalTransform;
    public GameObject fullScreenButton;
    public RawImage Image;
    private VideoPlayer Player;
    private bool firstPlay = false;
    private AudioSource audioSource;
    

    private void Start()
    {
        mTrackableBehaviour = GetComponent<TrackableBehaviour>();
        if (mTrackableBehaviour)
        {
            mTrackableBehaviour.RegisterTrackableEventHandler(this);
        }
        _originalTransform = ObjectToDetect.transform;
       
       
    }

    

    public void OnTrackableStateChanged(TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus)
    {
       
        if (newStatus == TrackableBehaviour.Status.DETECTED ||
            newStatus == TrackableBehaviour.Status.TRACKED ||
            newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
            OnTrackingFound();
        }
        else
        {
            OnTrackingLost();
        }
    }


    private void OnTrackingFound()
    {
        Debug.Log(this.gameObject.transform.position);
        Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);
        Collider[] colliderComponents = GetComponentsInChildren<Collider>(true);
        fullScreenButton.SetActive(true);
        Image.enabled = true;
      
        if (!firstPlay)
        {
            StartCoroutine(playVideo());
            firstPlay = true;
        }
        
       
       // Player.enabled = true;
        if (Player!=null && firstPlay)
        {
            Player.Play(); 
        }
      
        // Enable rendering:
        foreach (Renderer component in rendererComponents)
        {
            component.enabled = true;
        }

        // Enable colliders:
        foreach (Collider component in colliderComponents)
        {
            component.enabled = true;
        }

        Debug.Log("Trackable " + mTrackableBehaviour.TrackableName + " found");
       // StartCoroutine(ScaleOverTime(2f));

    }


    private void OnTrackingLost()
    {
        fullScreenButton.SetActive(false);
        Image.enabled = false;
        if (Player!=null)
        {
            Player.Pause();
            //Player.enabled = false;
        }
       
        Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);
        Collider[] colliderComponents = GetComponentsInChildren<Collider>(true);

        // Disable rendering:
        foreach (Renderer component in rendererComponents)
        {
            component.enabled = false;
        }

        // Disable colliders:
        foreach (Collider component in colliderComponents)
        {
            component.enabled = false;
        }

        Debug.Log("Trackable " + mTrackableBehaviour.TrackableName + " lost");
    }
   
    public IEnumerator playVideo()
    {
      
        //Add VideoPlayer to the GameObject
        Player = gameObject.AddComponent<VideoPlayer>();
 
        //Add AudioSource
        audioSource = gameObject.AddComponent<AudioSource>();
 
        //Disable Play on Awake for both Video and Audio
        Player.playOnAwake = false;
        audioSource.playOnAwake = false;
        audioSource.Pause();
 
        //We want to play from video clip not from url
        
        //  Player.source = VideoSource.Url;
 
        // Vide clip from Url
        Player.source = VideoSource.Url;
        Player.url = "http://www.quirksmode.org/html5/videos/big_buck_bunny.mp4";
 
 
        //Set Audio Output to AudioSource
        Player.audioOutputMode = VideoAudioOutputMode.AudioSource;
 
        //Assign the Audio from Video to AudioSource to be played
        Player.EnableAudioTrack(0, true);
        Player.SetTargetAudioSource(0, audioSource);
 
        //Set video To Play then prepare Audio to prevent Buffering
        //Player.clip = videoToPlay;
        Player.Prepare();
 
        //Wait until video is prepared
        WaitForSeconds waitTime = new WaitForSeconds(1);
        while (!Player.isPrepared)
        {
            Debug.Log("Preparing Video");
            //Prepare/Wait for 5 sceonds only
            yield return waitTime;
            //Break out of the while loop after 5 seconds wait
            break;
        }
 
        Debug.Log("Done Preparing Video");
 
        //Assign the Texture from Video to RawImage to be displayed
        Image.texture = Player.texture;
 
        //Play Video
        Player.Play();
	  
 		
        Debug.Log("Playing Video");
        Player.isLooping = true;
	    
        Debug.Log("Done Playing Video");
    }
}