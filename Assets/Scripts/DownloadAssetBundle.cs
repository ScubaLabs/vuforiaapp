﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System;
using UnityEngine;
using System.Collections;
using System.IO;
using Lean.Touch;
using UnityEngine.UI;

public class DownloadAssetBundle : MonoBehaviour {
	
	#region Public Variables
	public string BundleURL;
	public string AssetName;
	public GameObject parentObject;
	public GameObject objectToAugment;
	public Boolean IsObjectDownloaded;
	
	public GameObject ListContainer;
	public GameObject listItem;
	
	
	public AssetBundle bundle;

	#endregion


	#region Private Variables

	#endregion
	
	IEnumerator Start() {
		
		// Download the file from the URL. It will not be saved in the Cache
		using (WWW www = new WWW(BundleURL)) {
			yield return www;
			if (www.error != null)
				throw new Exception("WWW download had an error:" + www.error);
			else
			{
				bundle = www.assetBundle;
				Debug.Log(www.bytesDownloaded);
				
				String[] modelsName = bundle.GetAllAssetNames();
		
				for (int i = 0; i < modelsName.Length; i++)
				{
					GameObject row = Instantiate(listItem)as GameObject;
					Text modelName = row.GetComponentInChildren<Text>();
					int LastIndex = (modelsName[i].LastIndexOf(".")-1)-modelsName[i].LastIndexOf("/");
					String correctName = modelsName[i].Substring(modelsName[i].LastIndexOf("/")+1,LastIndex).ToUpper();
					modelName.text = correctName;
					row.name = correctName;
					row.transform.SetParent(ListContainer.transform);
					row.transform.localPosition=Vector3.zero;
					row.transform.localRotation=Quaternion.identity;
					row.transform.localScale=Vector3.one;
					row.GetComponent<ShowSelectedModel>().parentObject = parentObject;
					row.GetComponent<ShowSelectedModel>().bundle=bundle;
				}
				
			
			}

		} // memory is freed from the web stream (www.Dispose() gets called implicitly)
	}

	

	

	
}
