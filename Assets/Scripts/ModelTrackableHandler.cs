﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using Vuforia;

public class ModelTrackableHandler : MonoBehaviour, ITrackableEventHandler
{
    #region PRIVATE_MEMBER_VARIABLES
 
    private TrackableBehaviour mTrackableBehaviour;
    
    #endregion // PRIVATE_MEMBER_VARIABLES
    
    private GameObject ObjectToDetect;
    private Transform _originalTransform;
    private DownloadAssetBundle _assetBundleObject;
    private List<string>modelsList;
    private AssetBundle modelsBundle;
    public GameObject shareButtons;
    public GameObject ScannerAnimator;
    public string test;
    private void Start()
    {
        mTrackableBehaviour = GetComponent<TrackableBehaviour>();
        if (mTrackableBehaviour)
        {
            mTrackableBehaviour.RegisterTrackableEventHandler(this);
        }
        modelsList=new List<string>();
        
    }

    

    public void OnTrackableStateChanged(TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus)
    {
       
        if (newStatus == TrackableBehaviour.Status.DETECTED ||
            newStatus == TrackableBehaviour.Status.TRACKED ||
            newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
            OnTrackingFound();
        }
        else
        {
            OnTrackingLost();
        }
    }


    private void OnTrackingFound()
    {
       // _assetBundleObject.populateList();
        StopScanning();
       
           
            Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);
            Collider[] colliderComponents = GetComponentsInChildren<Collider>(true);

        



            // Enable rendering:
            foreach (Renderer component in rendererComponents)
            {
                component.enabled = true;
            }

            // Enable colliders:
            foreach (Collider component in colliderComponents)
            {
                component.enabled = true;
            }

            Debug.Log("Trackable " + mTrackableBehaviour.TrackableName + " found");

          //  StartCoroutine(ScaleOverTime(1f));
        
    }


    private void OnTrackingLost()
    {
       
        StartScanning();       
        
      
        Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);
        Collider[] colliderComponents = GetComponentsInChildren<Collider>(true);

        // Disable rendering:
        foreach (Renderer component in rendererComponents)
        {
            component.enabled = false;
        }

        // Disable colliders:
        foreach (Collider component in colliderComponents)
        {
            component.enabled = false;
        }

        Debug.Log("Trackable " + mTrackableBehaviour.TrackableName + " lost");
    }
   
    
    public void StartScanning()
    {
        if (ScannerAnimator!=null)
        {
            ScannerAnimator.SetActive(true);
            ScannerAnimator.GetComponent<Animator>().SetTrigger("start");
            shareButtons.SetActive(false);
        }
      
    }

    public void StopScanning()
    {
        if (ScannerAnimator!=null)
        {
            test = "stopped scanning";
            ScannerAnimator.SetActive(false);
            shareButtons.SetActive(true);
            ScannerAnimator.GetComponent<Animator>().SetTrigger("stop"); 
        }
       

    }
    
}