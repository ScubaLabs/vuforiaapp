﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void LoadScanScene()
	{ 
		SceneManager.LoadScene("MainScene");
		Debug.Log("Main Scene rendered");
	}

	public void LoadVideoScene()
	{
		SceneManager.LoadScene("VideoModel");
	}

	public void LoadWhoWeAreScene()
	{
		Application.OpenURL("https://www.google.com");
	}

	public void LoadContactUsScene()
	{
		
	}

	public void LoadProductScene()
	{
		
	}
}
