﻿using System.Collections;
using Lean.Touch;
using UnityEngine;

public class ShowSelectedModel : MonoBehaviour
{
    private GameObject objectToAugment;

    public GameObject parentObject;
      
    public AssetBundle bundle;
    private string name;
    private string parentName;
    
    
    public void onListItemClick(GameObject nameGameObject)
    {
        name = nameGameObject.transform.parent.name.ToLower();
         
        objectToAugment = (GameObject) bundle.LoadAsset(name);

        if (parentObject.transform.childCount>=1 || parentObject.transform.childCount==0)
        {
            if (parentObject.transform.childCount >= 1)
            {
                Destroy(parentObject.transform.GetChild(0).gameObject);
            }

            objectToAugment = Instantiate(objectToAugment, parentObject.transform);
            objectToAugment.transform.localRotation = new Quaternion(0, 90, 0, 1);
            objectToAugment.transform.localPosition = new Vector3(0, 0.1f, 0);
            objectToAugment.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
            objectToAugment.AddComponent<LeanScale>().RequiredFingerCount = 2;
            objectToAugment.AddComponent<LeanTranslate>().RequiredFingerCount = 1;
            parentName = objectToAugment.transform.parent.name;
            StartCoroutine(ScaleOverTime(1f));
        }

    }
    IEnumerator ScaleOverTime(float time)
    {
        Vector3 currentScale = new Vector3(0.05f,0.05f,0.05f);
        Vector3 destinationScale = objectToAugment.transform.localScale;
        float currentTime = 0.0f;
         
        do
        {
            objectToAugment.transform.localScale = Vector3.Lerp(currentScale, destinationScale, currentTime / time);
            currentTime += Time.deltaTime;
         
            yield return null;
        } while (currentTime <= time);
       
    }

   
}