﻿using UnityEngine;

public class ListManager : MonoBehaviour
{
    public GameObject list;

    public void populateList()
    {
        Debug.Log("clicked");
        if (list.activeInHierarchy)
        {
            list.SetActive(false);
        }
        else
        {
            list.SetActive(true);
        }
    }
}